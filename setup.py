from distutils.command import clean
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
from Cython.Build import cythonize
import numpy

ext_modules = [
    Extension("watertone",
              ["src/watertone.pyx"],
			  include_dirs=[numpy.get_include()],
              extra_compile_args=['-O3', '-mtune=native', '-march=native'])
]

setup(
    name='PyWatertone',
    version='0.1',
    url='http://www.sawp.com.br',
    license='GPLv2',
    author='pedro',
    author_email='sawp@sawp.com.br',
    description='Embed and watermark color images into halftones.',
    cmdclass={'build_ext': build_ext},
    ext_modules=ext_modules, requires=['numpy', 'scipy']
)
