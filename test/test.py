from scipy.misc import *
import halftones.halftone as hlt
import halftones.inverse_halftone as ihlt
from skimage.filter import rank
from skimage.morphology import disk
import watertone


def simulation_queiroz(color_image, scale=3.0):
    color_image = imresize(color_image, scale)
    marked_gray = watertone.encode_color_to_gray_queiroz(color_image)
    marked_halftone = hlt.error_diffusion_floyd_steinberg(marked_gray)
    inverse_halftone = rank.mean(marked_halftone, selem=disk(3))
    decoded_color = watertone.decode_gray_to_color_queiroz(inverse_halftone)
    decoded_color = imresize(decoded_color, 1.0 / scale)
    imsave('queiroz_1_color_to_gray.png', marked_gray)
    imsave('queiroz_2_marked_halftone.png', marked_halftone)
    imsave('queiroz_3_inverse_halftone.png', inverse_halftone)
    imsave('queiroz_4_gray_to_color.png', decoded_color)


def simulation_ko(color_image, scale=3.0):
    color_image = imresize(color_image, scale)
    marked_gray = watertone.encode_color_to_gray_ko(color_image)
    marked_halftone = hlt.error_diffusion_floyd_steinberg(marked_gray)
    inverse_halftone = rank.mean(marked_halftone, selem=disk(3))
    decoded_color = watertone.decode_gray_to_color_ko(inverse_halftone)
    decoded_color = imresize(decoded_color, 1.0 / scale)
    imsave('ko_1_color_to_gray.png', marked_gray)
    imsave('ko_2_marked_halftone.png', marked_halftone)
    imsave('ko_3_inverse_halftone.png', inverse_halftone)
    imsave('ko_4_gray_to_color.png', decoded_color)


def simulation_freitas_color_to_halftone(color_image):
    marked_halftone = watertone.encode_color_to_halftone_freitas(color_image)
    decoded_color = watertone.decode_halftone_to_color_freitas(marked_halftone)
    imsave('freitas_1_marked_halftone.png', marked_halftone)
    imsave('freitas_2_halftone_to_color.png', decoded_color)


def simulations_freitas_hardcopy_watermark(host, mark):
    marked = watertone.encode_halftone_watermark(host, mark)
    extracted = watertone.decode_halftone_watermark(marked)
    imsave('watermark_1_marked_halftone.png', marked)
    imsave('extracted_2_halftone_to_color.png', extracted)


if __name__ == "__main__":
    host = imread('Peppers.tiff')
    mark = imread('baboon.jpg')
    simulation_queiroz(host)
    simulation_ko(host)
    simulation_freitas_color_to_halftone(host)
    simulations_freitas_hardcopy_watermark(host, mark)
