Contents
========

1.  [Description](#description)
2.  [License](#license)
3.  [Requirements](#requirements)
4.  [Installation](#installation)
5.  [Instructions](#instructions)
6.  [Contact](#contact)
7.  [Contributing](#contributing)

Description
===========

Watertone is a python library that implements reversible methods to convert color graphics and pictures to printable images. Some implemented methods are based on wavelet-space watermarking. Other methods in this library are designed to watermark the color information directly on the printable halftones.

License
=======

Watertone is released under [GNU GPL version
2.](http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt)


Requirements
============

Watertone was tested with [Python 2.7](https://www.python.org/download/releases/2.7/) and [Python 3.4.0](https://www.python.org/download/releases/3.4.0/), [Scipy 0.10.0](http://www.scipy.org/), [Scikit-image 0.7.1](http://scikit-image.org/), [Numpy 1.9.0](http://www.numpy.org/), [PyWavelets](http://www.pybytes.com/pywavelets/), [Halftones](https://bitbucket.org/kuraiev/halftones), [PyColorTools](https://bitbucket.org/kuraiev/pycolortools), and [Cython 0.21.1](http://cython.org/).

Installation
===========

1. Clone the last version
> `# hg clone https://bitbucket.org/kuraiev/watertone`
2. Go to the directory where 'setup.py' os located
> `cd watertone`
3. Install using distributils
> `# sudo python setup.py install`

Instructions
============

The repository 'test' contains some examples of how to use the functions.

Contact
=======

Please send all comments, questions, reports and suggestions (especially if you would like to contribute) to **sawp@sawp.com.br**

Contributing
============

If you would like to contribute with new algorithms, increment the code performance, documentation or another kind of modifications, please contact me. The only requirements are: keep the code compatible with
PEP8 standardization and licensed by GPLv2.

References
==========

[1]  de Queiroz, Ricardo L., and Karen M. Braun. **Color to gray and back: color embedding into textured gray images.** Image Processing, IEEE transactions on 15.6 (2006): 1464-1470

[2] Ko, Kyung-Woo, et al. **Color embedding and recovery based on wavelet packet transform.** Journal of Imaging Science and Technology 52.1 (2008): 10501-1.
