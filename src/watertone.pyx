# encoding: utf-8
# cython: profile=True
# filename: watertone.pyx

"""
This module implements some techniques of reversible method to convert
color graphics and pictures to gray (or halftone) images.
"""

import itertools
import pywt
import numpy as np
import halftones.halftone as hlt
import halftones.inverse_halftone as ihlt
import scipy.spatial.distance as dist
from scipy.interpolate import interp1d
from scipy.misc import imresize
from color_tools import rgb2ycbcr_fullrange, ycbcr2rgb_fullrange
from color_tools import split_channels, join_channels
from collections import OrderedDict
from skimage.util.shape import view_as_blocks
from skimage.color import rgb2gray
from abc import ABCMeta

cimport numpy as np


class MaskManager:
    """
    MaskManager encapulate the masks used in Freitas' method for embed
    watermarks in a printable halftone image.

    """
    def __init__(self):
        self.__combine_masks()
        self.__reverse_masks()

    def coding_masks(self, color=None):
        if color is None:
            return self.__combinations
        else:
            return self.__combinations[color]

    def decoding_masks(self, mask=None):
        if mask is None:
            return self.__reverted_mask
        else:
            if type(mask) is np.ndarray:
                k = tuple(mask.flatten().tolist())
            elif type(mask) is list:
                k = tuple(mask)
            else:
                k = mask
            return self.__reverted_mask[k]

    @staticmethod
    def __create_masks(size=9):
        it = itertools.product([0, 1], repeat=size)
        dims = [np.sqrt(i).astype(np.uint8) for i in (size, size)]
        return [np.array(i).reshape(dims) for i in it]

    def __combine_masks(self):
        masks = self.__create_masks()
        combinations = self.__create_combinations()
        self.__combinations = self.__join_masks(masks, combinations)

    def __reverse_masks(self):
        joined_masks = self.__combinations
        reverse = {}
        for k in joined_masks:
            for mask in joined_masks[k]:
                key = tuple(mask.flatten().tolist())
                reverse[key] = k
        self.__reverted_mask = reverse

    @staticmethod
    def __map_maks(masks):
        mapped = {}
        for m in masks:
            s = m.sum()
            if s in mapped:
                mapped[s].append(m)
            else:
                mapped[s] = [m]
        return mapped

    def __join_masks(self, masks, combinations):
        counter = 0
        maped_masks = self.__map_maks(masks)
        keylist = list(combinations.keys())
        for m in maped_masks:
            while maped_masks[m]:
                if counter < len(combinations):
                    num_mask = maped_masks[m].pop()
                    mask = num_mask.astype(np.bool)
                    key = keylist[counter]
                    combinations[key].append(mask)
                    counter += 1
                else:
                    counter = 0
        return combinations

    @staticmethod
    def __create_combinations():
        combinations = {
            (0, 0, 0): [],
            (0, 0, 1): [],
            (0, 1, 0): [],
            (0, 1, 1): [],
            (1, 0, 0): [],
            (1, 0, 1): [],
            (1, 1, 0): [],
            (1, 1, 1): []
        }
        return OrderedDict(sorted(combinations.items()))


class Decoder:
    """
    Decoder is used to extract the color image embedded into the
    coded halftone using Freitas' method.
    """
    def __init__(self, coded, masks=MaskManager()):
        self.__mm = MaskManager()
        hR, hG, hB = self.__decoding_blocks(coded)
        iR, iG, iB = [self.__inverse_halftone(i) for i in (hR, hG, hB)]
        self.__restored = np.dstack((iR, iG, iB))

    @property
    def unmasked(self):
        return self.__restored

    @staticmethod
    def __inverse_halftone(channel):
        return ihlt.inverse_fbih(channel)

    def __decoding_blocks(self, coded, size=3):
        blocks = view_as_blocks(coded, block_shape=(size, size))
        (h, w, self.__h_block, self.__w_block) = blocks.shape
        hR = np.zeros((h, w))
        hG = np.zeros((h, w))
        hB = np.zeros((h, w))
        for i in range(h):
            for j in range(w):
                mask = blocks[i, j]
                decoded = self.__mm.decoding_masks(mask)
                hR[i, j] = decoded[0]
                hG[i, j] = decoded[1]
                hB[i, j] = decoded[2]
        return hR, hG, hB


class Coder:
    """
    Coder is used to create a printable black-and-white halftone image which
    embeds a color image using Freitas' method.

    """
    def __init__(self, host, mark, hlt_fun_rgb=hlt.error_diffusion_jarvis,
                 hlt_fun_host=hlt.error_diffusion_jarvis,
                 distance_criteria=dist.dice, masks=MaskManager()):
        self.dist = distance_criteria
        self.__mm = masks
        self.hlt_fun_rgb = hlt_fun_rgb
        self.hlt_fun_host = hlt_fun_host
        self.R, self.G, self.B = split_channels(mark)
        self.gray = host
        self.halftone_blocks = self.__padded_array(self.gray)
        self.hR = self.__halftone(self.R)
        self.hG = self.__halftone(self.G)
        self.hB = self.__halftone(self.B)
        self.__convert_dithering()

    @property
    def masked(self):
        h, w = self.gray.shape
        masks = self.__masked
        lines = []
        for i in range(h):
            lines.append(np.concatenate(masks[i * w:i * w + w], axis=1))
        return np.concatenate(lines, axis=0)

    def __padded_array(self, arr, size=3):
        reference_halft = imresize(arr, float(size))
        half = self.hlt_fun_host(reference_halft).astype(np.bool)
        return view_as_blocks(half, block_shape=(size, size))

    def __halftone(self, channel):
        return self.hlt_fun_rgb(channel).astype(np.bool)

    def __get_best_dither(self, t, size=3):
        (x, y, r, g, b) = t
        masks = self.__mm.coding_masks((r, g, b))
        reference = self.halftone_blocks[x, y]
        bm = self.__get_best_match(reference, masks)
        return bm

    def __get_best_match(self, ref, masks):
        similarity_indexes = [self.__get_simmilarity(ref, m) for m in masks]
        more_similar = self.__get_more_simmilar_index(similarity_indexes)
        return masks[more_similar]

    @staticmethod
    def __get_more_simmilar_index(simmilar_index):
        return simmilar_index.index(min(simmilar_index))

    def __get_simmilarity(self, reference, mask):
        try:
            if np.all(mask == False) and np.all(reference == False):
                return 0
            else:
                return self.dist(reference.flatten(), mask.flatten())
        except ZeroDivisionError:
            return 0

    def __convert_dithering(self):
        (h, w) = self.gray.shape
        codes = []
        for i in range(h):
            for j in range(w):
                r, g, b = self.hR[i, j], self.hG[i, j], self.hB[i, j]
                codes.append((i, j, r, g, b))
        self.__masked = [self.__get_best_dither(c) for c in codes]


class WaveletCodec:
    """
    WaveletCodec is an abstract class that encapsulates the wavelet type. It
    is used by watermarking techniques that use wavelet domain.

    """
    __metaclass__ = ABCMeta

    def __init__(self):
        self._wavelet = 'haar'

    def set_wavelet_type(self, wavelet_type='haar'):
        self._wavelet = wavelet_type


class CoderQueiroz(WaveletCodec):
    """
    CoderQueiroz is used to create a grayscale image which
    embeds a color image using Queiroz' method.

     References
    ----------
    .. [1] de Queiroz, Ricardo L., and Karen M. Braun. *Color to gray and
           back: color embedding into textured gray images.* Image Processing,
           IEEE transactions on 15.6 (2006): 1464-1470.

    """
    def encode(self, np.ndarray rgb):
        cdef np.ndarray y, cb, cr
        cdef np.ndarray ycbcr = rgb2ycbcr_fullrange(rgb)
        y, cb, cr = split_channels(ycbcr)
        y = __interp_0255to01(y)
        cdef np.ndarray marked_gray = self.__mark_channels(y, cb, cr)
        cdef np.ndarray normalized_gray = __interp_normalizeto0255(marked_gray)
        return normalized_gray

    def __split_cb(self, np.ndarray cb):
        cdef np.ndarray cbminus = self.__get_cbminus(cb)
        cdef np.ndarray cbplus = self.__get_cbplus(cb)
        return cbminus, cbplus

    @staticmethod
    def __get_cbminus(np.ndarray cb):
        cdef np.ndarray cbminus = imresize(cb, 0.25)
        cbminus = __interp_0255to11(cbminus)
        cbminus[cbminus > 0] = 0
        return cbminus

    @staticmethod
    def __get_cbplus(np.ndarray cb):
        cdef np.ndarray cbplus = imresize(cb, 0.5)
        cbplus = __interp_0255to11(cbplus)
        cbplus[cbplus < 0] = 0
        return cbplus

    def __split_cr(self, np.ndarray cr):
        cdef np.ndarray crminus = self.__get_crminus(cr)
        cdef np.ndarray crplus = self.__get_crplus(cr)
        return crminus, crplus

    def __get_crplus(self, np.ndarray cr):
        return self.__get_cbplus(cr)

    def __get_crminus(self, np.ndarray cr):
        cdef np.ndarray resizedcr = imresize(cr, 2.0)
        return self.__get_cbminus(resizedcr)

    def __mark_channels(self, np.ndarray y, np.ndarray cb, np.ndarray cr):
        cdef np.ndarray marked, chan1
        cdef np.ndarray crminus, crplus, cbminus, cbplus
        cdef np.ndarray ca, ch2, cv2, cd2, ca1, ch1, cv1, cd1
        crminus, crplus, = self.__split_cr(cr)
        cbminus, cbplus = self.__split_cb(cb)
        (ca, (ch2, cv2, cd2)) = pywt.dwt2(y, self._wavelet)
        (ca1, (ch1, cv1, cd1)) = pywt.dwt2(ca, self._wavelet)
        chan1 = pywt.idwt2((ca1, (ch1, cv1, cbminus)), self._wavelet)
        marked = pywt.idwt2((chan1, (crplus, cbplus, crminus)), self._wavelet)
        return marked


class DecoderQueiroz(WaveletCodec):
    """
    DecoderQueiroz is used to recover the color image from a coded (textured)
    grayscale image using Queiroz' method.

     References
    ----------
    .. [1] de Queiroz, Ricardo L., and Karen M. Braun. *Color to gray and
           back: color embedding into textured gray images.* Image Processing,
           IEEE transactions on 15.6 (2006): 1464-1470.

    """
    def decode(self, np.ndarray marked_gray):
        cdef np.ndarray y, cb, cr, ycbcr, rgb, gray
        gray = __interp_normalizeto01(marked_gray)
        y, cb, cr = self.__extract_color_channels(gray)
        ycbcr = join_channels(y, cb, cr)
        rgb = ycbcr2rgb_fullrange(ycbcr)
        rgb = __interp_normalizeto0255(rgb)
        return rgb

    def __extract_color_channels(self, np.ndarray gray):
        cdef np.ndarray chan1, crplus, cbplus, crminus
        cdef np.ndarray ca1, ch1, cv1, cbminus
        cdef np.ndarray y, Cb, Cr
        chan1, (crplus, cbplus, crminus) = pywt.dwt2(gray, self._wavelet)
        ca1, (ch1, cv1, cbminus) = pywt.dwt2(chan1, self._wavelet)
        y = self.__get_y(ca1, ch1, cv1)
        cbminus = self.__resize_cbminus(cbminus)
        cb = self.__get_cb(cbminus, cbplus)
        cr = self.__get_cr(crminus, crplus)
        return y, cb, cr

    @staticmethod
    def __resize_cbminus(np.ndarray cbminus):
        cbminus = __interp_11to0255(cbminus)
        cbminus = cbminus.astype(np.uint8)
        cbminus = imresize(cbminus, 2.0)
        cbminus = __interp_0255to11(cbminus)
        return cbminus

    def __get_cb(self, np.ndarray cbminus, np.ndarray cbplus):
        cdef np.ndarray cb = np.abs(cbplus) - np.abs(cbminus)
        cb = __interp_11to0255(cb)
        cb = cb.astype(np.uint8)
        cb = imresize(cb, 2.0)
        return cb

    def __get_cr(self, np.ndarray crminus, np.ndarray crplus):
        return self.__get_cb(crminus, crplus)

    def __get_y(self, np.ndarray ca1, np.ndarray ch1, np.ndarray cv1):
        cdef np.ndarray z1 = np.zeros((ch1.shape[0], ch1.shape[1]))
        cdef np.ndarray z2 = np.zeros((2 * ch1.shape[0], 2 * ch1.shape[1]))
        cdef np.ndarray y = pywt.idwt2((ca1, (ch1, cv1, z1)), self._wavelet)
        y = pywt.idwt2((y, (z2, z2, z2)), self._wavelet)
        y = __interp_normalizeto0255(y)
        y = y.astype(np.uint8)
        return y


class CoderKo(WaveletCodec):
    """
    CoderKo is used to create a grayscale image which
    embeds a color image using Ko' method.

     References
    ----------
    .. [1] Ko, Kyung-Woo, et al. *Color embedding and recovery based on
           wavelet packet transform.* Journal of Imaging Science and
           Technology 52.1 (2008): 10501-1.

    """
    def encode(self, np.ndarray rgb):
        y, cb, cr = self.__get_ycbcr(rgb)
        marked = self.__mark(y, cb, cr)
        marked_gray = __interp_normalizeto0255(marked)
        return marked_gray

    def __mark(self, y, cb, cr):
        packets = pywt.WaveletPacket2D(y, self._wavelet)
        packets['hv'].data = cb
        packets['vh'].data = cr
        return packets.reconstruct()

    @staticmethod
    def __get_ycbcr(rgb):
        ycbcr = rgb2ycbcr_fullrange(rgb)
        y, cb, cr = split_channels(ycbcr)
        cb, cr = imresize(cb, 0.25), imresize(cr, 0.25)
        cb = __interp_0255to11(cb)
        cr = __interp_0255to11(cr)
        y = __interp_0255to01(y)
        return y, cb, cr


class DecoderKo(WaveletCodec):
    """
    DecoderKo is used to recover the color image from a coded (textured)
    grayscale image using Ko' method.

     References
    ----------
    .. [1] Ko, Kyung-Woo, et al. *Color embedding and recovery based on
           wavelet packet transform.* Journal of Imaging Science and
           Technology 52.1 (2008): 10501-1.

    """
    def decode(self, np.ndarray marked_gray):
        cdef np.ndarray y = __interp_0255to01(marked_gray)
        cdef np.ndarray ycbcr = self.__extract_ycbcr(y)
        cdef np.ndarray rgb = ycbcr2rgb_fullrange(ycbcr)
        rgb = __interp_normalizeto0255(rgb).astype(np.uint8)
        return rgb

    def __extract_ycbcr(self, np.ndarray marked_gray):
        packets = pywt.WaveletPacket2D(marked_gray, self._wavelet)
        cdef np.ndarray cb = self.__extract_cb(packets)
        cdef np.ndarray cr = self.__extract_cr(packets)
        cdef np.ndarray y = self.__extract_y(packets)
        cdef np.ndarray ycbcr = join_channels(y, cb, cr)
        return ycbcr

    @staticmethod
    def __extract_y(packets):
        cdef np.ndarray z = np.zeros(packets['hv'].data.shape)
        packets['hv'].data = z
        packets['vh'].data = z
        cdef np.ndarray y = packets.reconstruct()
        y = __interp_normalizeto0255(y)
        return y

    @staticmethod
    def __extract_cb(packets):
        cdef np.ndarray cb = packets['hv'].data
        cb = __interp_11to0255(cb).astype(np.uint8)
        cb = imresize(cb, 4.0)
        return cb

    @staticmethod
    def __extract_cr(packets):
        cdef np.ndarray cr = packets['vh'].data
        cr = __interp_11to0255(cr).astype(np.uint8)
        cr = imresize(cr, 4.0)
        return cr


# Helper functions
cdef __interp_11to0255(c):
    """
    Interpolate a 1-D linear function from [-1,1] to [0,255].

    Parameters
    ----------
    channel: numpy.ndarray
        Single image channel

    Returns
    -------
    interpolated: numpy.ndarray
        Channel with pixels adjusted to new range.

    """
    return interp1d([-1, 1], [0, 255], kind='linear')(c)

cdef __interp_01to0255(c):
    """
    Interpolate a 1-D linear function from [0,1] to [0,255].

    Parameters
    ----------
    channel: numpy.ndarray
        Single image channel

    Returns
    -------
    interpolated: numpy.ndarray
        Channel with pixels adjusted to new range.

    """
    return interp1d([0, 1], [0, 255], kind='linear')(c)

cdef __interp_0255to11(c):
    """
    Interpolate a 1-D linear function from [0,255] to [-1,1].

    Parameters
    ----------
    channel: numpy.ndarray
        Single image channel

    Returns
    -------
    interpolated: numpy.ndarray
        Channel with pixels adjusted to new range.

    """
    return interp1d([0, 255], [-1, 1], kind='linear')(c)

cdef __interp_0255to01(c):
    """
    Interpolate a 1-D linear function from [0,255] to [0,1].

    Parameters
    ----------
    channel: numpy.ndarray
        Single image channel

    Returns
    -------
    interpolated: numpy.ndarray
        Channel with pixels adjusted to new range.

    """
    return interp1d([0, 255], [0, 1], kind='linear')(c)

cdef __interp_normalizeto0255(c):
    """
    Normalize a matrix to range [0,255].

    Parameters
    ----------
    channel: numpy.ndarray
        Single image channel

    Returns
    -------
    interpolated: numpy.ndarray
        Channel with pixels adjusted to new range.

    """
    return interp1d([c.min(), c.max()], [0, 255], kind='linear')(c)

cdef __interp_normalizeto01(c):
    """
    Normalize a matrix to range [0,1].

    Parameters
    ----------
    channel: numpy.ndarray
        Single image channel

    Returns
    -------
    interpolated: numpy.ndarray
        Channel with pixels adjusted to new range.

    """
    return interp1d([c.min(), c.max()], [0, 1], kind='linear')(c)

cdef __rgb2gray(np.ndarray img):
    """
    Convert a 3-channel RGB image to 1-channel gray image.

    Parameters
    ----------
    color: numpy.ndarray
         Image with 3-channels space.

    Returns
    -------
    gray: numpy.ndarray
        Gray image with 1-channel space.

    """
    return (rgb2gray(img) * 255).astype(np.uint8)

cdef __encode_freitas(np.ndarray host, np.ndarray mark):
    """
    Embed the color image 'mark' into a grayscale 'host' and return
    a halftone image.

    marked_halftone = __encode_freitas(host, mark)

    Parameters
    ----------
    host: numpy.ndarray
        Image to host the mark.
    mark: numpy.ndarray
        Image which will be embedded into host.

    Returns
    -------
    marked_halftone: numpy.ndarray
        Black-and-white halftone of 'host' that embeds 'mark'.

    See Also
    --------
    __decode_freitas

    """
    c = Coder(host, mark)
    return c.masked

cdef __decode_freitas(np.ndarray coded_halftone):
    """
    Extract the hidden color image from a watermarked halftone.

    color_image = __decode_freitas(coded_halftone)

    Parameters
    ----------
    coded_halftone: numpy.ndarray
        Black-and-white halftone that contains a watermarked color image.

    Returns
    -------
    color_image: numpy.ndarray
        Color image extracted and decoded from coded_halftone.

    See Also
    --------
    __encode_freitas

    """
    d = Decoder(coded_halftone)
    return d.unmasked

# Library functions
cpdef encode_halftone_watermark(np.ndarray host, np.ndarray mark=None):
    """
    Encode (embed) a color image into a another channel and return a halftone.

    coded_halftone = encode_halftone_watermark(host, mark)

    Parameters
    ----------
    host: numpy.ndarray
        Color (3-channels) or gray (1-channel) image with pixels in [0, 255].
    mark: numpy.ndarray
        Color image which will be embedded into host.

    Returns
    -------
    marked_halftone: numpy.ndarray
        Black-and-white halftone of 'host' that embeds 'mark'.

    See Also
    --------
    decode_halftone_watermark

    """
    cdef np.ndarray rgb, gray
    if host.ndim == 3 and mark is None:
        rgb = host
        gray = __rgb2gray(host)
    elif host.ndim == 3 and mark.ndim == 3:
        gray = __rgb2gray(host)
        rgb = mark
    elif host.ndim == 1 and mark.ndim == 3:
        gray = host
        rgb = mark
    else:
        raise Exception("Wrong input parameters")
    return __encode_freitas(gray, rgb)

cpdef decode_halftone_watermark(np.ndarray coded_halftone):
    """
    Extract the hidden color image from a watermarked halftone.

    color_image = decode_halftone_watermark(coded_halftone)

    Parameters
    ----------
    coded_halftone: numpy.ndarray
        Black-and-white halftone that contains a watermarked color image.

    Returns
    -------
    color_image: numpy.ndarray
        Color image extracted and decoded from coded_halftone.

    See Also
    --------
    encode_halftone_watermark

    """
    halftone = coded_halftone.astype(np.bool)
    return __decode_freitas(halftone)

cpdef encode_color_to_halftone_freitas(np.ndarray rgb):
    """
    Embed the RGB image into its halftoned version using Freitas' method.

    coded_halftone = encode_color_to_halftone_freitas(rgb)

    Parameters
    ----------
    rgb: numpy.ndarray
        Color RGB image.

    Returns
    -------
    coded_halftone: numpy.ndarray
        Black-and-white halftone image containing the its color channels.

    See Also
    --------
    decode_halftone_to_color_freitas

    """
    return encode_halftone_watermark(rgb)

cpdef decode_halftone_to_color_freitas(np.ndarray halftone):
    """
    Extract the RGB color image from a coded halftone using Freitas' method.

    rgb = decode_halftone_to_color_freitas(halftone)

    Parameters
    ----------
    halftone: numpy.ndarray
        Black-and-white halftone that contains an embedded color image.

    Returns
    -------
    rgb: numpy.ndarray
        Color RGB image.

    """
    return decode_halftone_watermark(halftone)

cpdef encode_color_to_gray_queiroz(np.ndarray rgb, wavelet_type='haar'):
    """
    Embed the RGB image into its grayscale version using Queiroz' method.

    coded_halftone = encode_color_to_gray_queiroz(rgb, wavelet_type)

    Parameters
    ----------
    rgb: numpy.ndarray
        Color RGB image.
    wavelet_type: string (see pywt.wavelist)
        Wavelet type.

    Returns
    -------
    coded_gray: numpy.ndarray
        Textured grayscale image with embedded color channels.

    References
    ----------
    .. [1] de Queiroz, Ricardo L., and Karen M. Braun. *Color to gray and
           back: color embedding into textured gray images.* Image Processing,
           IEEE transactions on 15.6 (2006): 1464-1470.

    See Also
    --------
    decode_gray_to_color_queiroz

    """
    coder = CoderQueiroz()
    coder.set_wavelet_type(wavelet_type)
    return coder.encode(rgb)

cpdef decode_gray_to_color_queiroz(np.ndarray gray, wavelet_type='haar'):
    """
    Extract the hidden color image from a watermarked gray image using
    Queiroz's method.

    color_image = decode_gray_to_color_queiroz(coded_halftone, wavelet_type)

    Parameters
    ----------
    gray: numpy.ndarray
        Textured grayscale image with embedded color channels.

    Returns
    -------
    rgb: numpy.ndarray
        Recovered RGB image.
    wavelet_type: string (see pywt.wavelist)
        Wavelet type.

    References
    ----------
    .. [1] de Queiroz, Ricardo L., and Karen M. Braun. *Color to gray and
           back: color embedding into textured gray images.* Image Processing,
           IEEE transactions on 15.6 (2006): 1464-1470.

    See Also
    --------
    encode_color_to_gray_queiroz

    """
    decoder = DecoderQueiroz()
    decoder.set_wavelet_type(wavelet_type)
    return decoder.decode(gray)

cpdef encode_color_to_gray_ko(np.ndarray rgb, wavelet_type='haar'):
    """
    Embed the RGB image into its grayscale version using Ko's method.

    coded_halftone = encode_color_to_gray_ko(rgb, wavelet_type)

    Parameters
    ----------
    rgb: numpy.ndarray
        Color RGB image.
    wavelet_type: string (see pywt.wavelist)
        Wavelet type.

    Returns
    -------
    coded_gray: numpy.ndarray
        Textured grayscale image with embedded color channels.

    References
    ----------
    .. [1] Ko, Kyung-Woo, et al. *Color embedding and recovery based on
           wavelet packet transform.* Journal of Imaging Science and
           Technology 52.1 (2008): 10501-1.

    See Also
    --------
    decode_gray_to_color_ko

    """
    coder = CoderKo()
    coder.set_wavelet_type(wavelet_type)
    return coder.encode(rgb)

cpdef decode_gray_to_color_ko(np.ndarray marked_gray, wavelet_type='haar'):
    """
    Extract the hidden color image from a watermarked gray image using
    Ko's method.

    color_image = decode_gray_to_color_ko(coded_halftone, wavelet_type)

    Parameters
    ----------
    gray: numpy.ndarray
        Textured grayscale image with embedded color channels.
    wavelet_type: string (see pywt.wavelist)
        Wavelet type.

    Returns
    -------
    rgb: numpy.ndarray
        Recovered RGB image.

    References
    ----------
    .. [1] Ko, Kyung-Woo, et al. *Color embedding and recovery based on
           wavelet packet transform.* Journal of Imaging Science and
           Technology 52.1 (2008): 10501-1.

    See Also
    --------
    encode_color_to_gray_queiroz

    """
    decoder = DecoderKo()
    decoder.set_wavelet_type(wavelet_type)
    return decoder.decode(marked_gray)
